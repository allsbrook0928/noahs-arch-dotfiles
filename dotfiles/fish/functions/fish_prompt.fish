function fish_prompt
    set_color magenta 
    echo -n (basename $PWD)
    set_color cyan 
    echo -n ' ~ '
end
