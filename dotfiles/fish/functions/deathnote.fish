function deathnote --wraps='ani-cli death note' --wraps='ani-cli --dub death note' --description 'alias deathnote=ani-cli --dub death note'
  ani-cli --dub death note $argv
        
end
